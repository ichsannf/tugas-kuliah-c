#include <stdio.h>
#include <stdlib.h>

int main() {
  // Deklarasi variabel
  int i = 0;
  float data = 0.0, rata = 0.0, total = 0.0;

  // Program perulangan
  do {
    printf("Masukan data ke-%d: ", i + 1);
    scanf("%f", &data);
    if (data != 0) {
      total += data;
      i++;
    } else {
      break;
    }
  } while (1);

  rata = total / i;


  //Output Program
  printf("Banyaknya data: %d\n", i);
  printf("Total nilai data: %.2f\n", total);
  printf("Rata-rata nilai data: %.2f\n", rata);

  return 0;
}
