

/*
Nama	: ichsan Nur Fadilah
NIM		: 302230007
*/

#include <stdio.h>

int faktorial(int n) {
  int f = 1;
  for (int i = 1; i <= n; i++) {
    f *= i;
  }
  return f;
}

int kombinasi(int n, int r) {
  return faktorial(n) / (faktorial(n - r) * faktorial(r));
}

int main() {
  int n, i, j, k;
  printf("Masukkan N: ");
  scanf("%d", &n);

  for (i = 0; i < n; i++) {
    printf(" ");
    for (j = 0; j <= n - i; j++) {
      printf(" ");
    }
    for (k = 0; k <= i; k++) {
      if (k == 0 || k == i) {
        printf("%d", kombinasi(i, k));
        printf(" ");
      } else {
        printf("%d", kombinasi(i, k));
        printf(" ");
      }
    }
    printf("\n");
  }
  return 0;
}

/*
Masukkan N: 5
       1
      1 1
     1 2 1
    1 3 3 1
   1 4 6 4 1

--------------------------------
Process exited after 7.403 seconds with return value 0
Press any key to continue . . .

*/
