#include <stdio.h>

#define MAX_MAHASISWA 100

struct Mahasiswa {
  char nama[50];
  double nilai_absen;
  double nilai_uts;
  double nilai_tugas;
  double nilai_quiz;
  double nilai_uas;
  double nilai_akhir;
  char huruf_mutu;
};

int main() {
  int n_mahasiswa;
  struct Mahasiswa mhs[MAX_MAHASISWA];

  // Membaca jumlah mahasiswa
  printf("\n******************************\n");
  printf("\nPROGRAM HITUNG NILAI MAHASISWA\n");
  printf("\n******************************\n");
  printf("\n=========================\n");
  printf("Masukkan jumlah mahasiswa: ");
  scanf("%d", &n_mahasiswa);

  // Membaca data setiap mahasiswa
  for (int i = 0; i < n_mahasiswa; i++) {
    printf("\n------------------------\n");
    printf("Mahasiswa ke-%d:\n", i + 1);
    printf("Nama: ");
    scanf("%s", mhs[i].nama);

    // Membaca nilai absen, uts, tugas, quiz, dan uas
    printf("Nilai Absen: ");
    scanf("%lf", &mhs[i].nilai_absen);

    printf("Nilai UTS: ");
    scanf("%lf", &mhs[i].nilai_uts);

    printf("Nilai Tugas: ");
    scanf("%lf", &mhs[i].nilai_tugas);

    printf("Nilai Quiz: ");
    scanf("%lf", &mhs[i].nilai_quiz);

    printf("Nilai UAS: ");
    scanf("%lf", &mhs[i].nilai_uas);

    // Menghitung nilai akhir
    mhs[i].nilai_akhir = (0.1 * mhs[i].nilai_absen) +
                         (0.2 * mhs[i].nilai_uts) +
                         (0.3 * mhs[i].nilai_tugas) +
                         (0.4 * mhs[i].nilai_quiz) +
                         (0.5 * mhs[i].nilai_uas);

    // Menentukan huruf mutu
    if (mhs[i].nilai_akhir > 85) {
      mhs[i].huruf_mutu = 'A';
    } else if (mhs[i].nilai_akhir > 70) {
      mhs[i].huruf_mutu = 'B';
    } else if (mhs[i].nilai_akhir > 55) {
      mhs[i].huruf_mutu = 'C';
    } else if (mhs[i].nilai_akhir > 40) {
      mhs[i].huruf_mutu = 'D';
    } else {
      mhs[i].huruf_mutu = 'E';
    }
  }

  // Menampilkan hasil
  for (int i = 0; i < n_mahasiswa; i++) {
    printf("\n------------------------\n");
    printf("Mahasiswa ke-%d:\n", i + 1);
    printf("Nama: %s\n", mhs[i].nama);
    printf("Nilai Akhir: %.2f\n", mhs[i].nilai_akhir);
    printf("Huruf Mutu: %c\n", mhs[i].huruf_mutu);
  }

  return 0;
}
