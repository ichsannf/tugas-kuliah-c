

#include <stdio.h>
#include <stdlib.h>
#include "myheader.h"



int main()
{
    system("clear");

    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    
    jdl_aplikasi();
    input(nama,&gol,status);
    gapok_tunja(gol,status,&gapok,&tunja);
    prosen_pot = prosen_potongan(gapok);
    pot = potongan(gapok,tunja,prosen_pot);
    gaber = (gapok + tunja) - pot;

    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);

    return 0;
}
