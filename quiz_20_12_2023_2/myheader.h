#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void jdl_aplikasi();

void input_data(char *nama, char *gol, char *status);

void hitung_gaji(char gol, char *status, float *gapok, float *tunja, float *prosen_pot, float *pot, float *gaber);

void output(float gapok, float tunja, float pot, float gaber);

#endif  // MYHEADER_H
