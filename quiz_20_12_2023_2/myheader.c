#include "myheader.h"

void jdl_aplikasi() {
    system("clear");
    printf("*************************\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*************************\n\n");
}

void input_data(char *nama, char *gol, char *status) {
    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", gol);
    printf("Status (Nikah\\Belum): ");
    scanf("%s", status);
}

void hitung_gaji(char gol, char *status, float *gapok, float *tunja, float *prosen_pot, float *pot, float *gaber) {
    switch (gol) {
        case 'A':
            *gapok = 200000;
            if (strcmp(status, "Nikah") == 0) {
                *tunja = 50000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    *tunja = 25000;
                }
            }
            break;
        case 'B':
            *gapok = 350000;
            if (strcmp(status, "Nikah") == 0) {
                *tunja = 75000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    *tunja = 60000;
                }
            }
            break;
        default:
            printf(" Salah Input Golongan !!!\n");
            exit(0);
    }

    *prosen_pot = (*gapok > 300000) ? 0.1 : 0.05;
    *pot = (*gapok + *tunja) * *prosen_pot;
    *gaber = (*gapok + *tunja) - *pot;
}

void output(float gapok, float tunja, float pot, float gaber) {
    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);
}
