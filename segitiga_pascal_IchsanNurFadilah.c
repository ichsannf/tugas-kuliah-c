#include <stdio.h>
// Deklarasi Variabel
int main() {
  int main() {
  int n, i, j;

  printf("Masukkan Nilai N ");
  scanf("%d", &n);
  //* Looping Program
  int segitiga[n][n];
  for (i = 0; i < n; i++) {
    for (j = 0; j < n; j++) {
      segitiga[i][j] = 0;
    }
  }
 // Isi 
  segitiga[0][0] = 1;
  for (i = 1; i < n; i++) {
    for (j = 0; j <= i; j++) {
      if (j == 0 || j == i) {
        segitiga[i][j] = 1;
      } else {
        segitiga[i][j] = segitiga[i - 1][j - 1] + segitiga[i - 1][j];
      }
    }
  }

  return 0;
}
